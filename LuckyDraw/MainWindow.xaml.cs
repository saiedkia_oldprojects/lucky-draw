﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace LuckyDraw
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // back image 0 ~ 10
        // front image 11 ~ 21


        List<string> lstSelected = new List<string>();
        public MainWindow()
        {
            InitializeComponent();
        }

        DataTable dt;
        private void btnReadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //var fileName = string.Format("{0}\\fileNameHere", Directory.GetCurrentDirectory());
                OpenFileDialog opdialog = new OpenFileDialog();
                opdialog.Filter = "Excel 2007 File (*.xls)|*.xls|Excel 2010 (*.xlsx)|*.xlsx";
                opdialog.ShowDialog();

                if (opdialog.FileName != "" && System.IO.Path.GetExtension(opdialog.FileName) == ".xls"
                    || System.IO.Path.GetExtension(opdialog.FileName) == ".xlsx")
                {
                    string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", opdialog.FileName);

                    var adapter = new OleDbDataAdapter("SELECT * FROM [sheet1$]", connectionString);
                    dt = new DataTable();
                    adapter.Fill(dt);

                    if (dt.Rows.Count > 0)
                        MessageBox.Show("Ok!" + System.Environment.NewLine + "Count: " + dt.Rows.Count);
                    else
                        MessageBox.Show("File is empty!");
                }

            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        string num = null;
        int next = -1;
        private void btnRandom_Click(object sender, RoutedEventArgs e)
        {
            if (dt != null && dt.Rows.Count > 0)
            {


                if (next == -1)
                {
                    next = 0;

                    while (true)
                    {
                        Random rnd = new Random();
                        next = rnd.Next(0, dt.Rows.Count - 1);


                        num = "0" + dt.Rows[next][0].ToString();
                        if (!lstSelected.Contains(num))
                            break;
                    }

                }
                else
                {
                    num = "0" + dt.Rows[next][0].ToString();
                    if (lstSelected.Contains(num))
                        throw new Exception("قبلا انخاب شده است!");
                }


                lstSelected.Add(num);
                lblRow.Content = "شماره ردیف : " + next.ToString();

                loadImages();

            }


        }

        private void loadImages()
        {
            int i = 0;
            foreach (char ch in num)
            {
                foreach (UIElement item in gNumbers.Children)
                {
                    Image img = item as Image;
                    if (img != null && img.Name == "img" + i.ToString())
                    {
                        img.Source = new BitmapImage(new Uri("/images/" + ch + ".png", UriKind.Relative));
                        img.UpdateLayout();
                    }
                }

                i++;
            }

            VisNumbers();
        }

        private void HidenNumbers()
        {
            num = null;
            next = -1;
            ShowedNum = 11;
            foreach (UIElement item in gNumbers.Children)
            {
                Image img = item as Image;
                if (img != null && img.Name == "img" + ShowedNum.ToString())
                {
                    img.Visibility = System.Windows.Visibility.Visible;
                    ShowedNum++;
                }
            }

            lblRow.Content = "شماره ردیف : " + "-";
        }

        int ShowedNum;
        public void VisNumbers()
        {
            ShowedNum = 11;
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Elapsed += (obj, args) =>
            {
                if (ShowedNum == 15)
                    ShowedNum = 18;


                timer.Enabled = false;

                gDraw.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        foreach (UIElement item in gNumbers.Children)
                        {
                            Image img = item as Image;
                            if (img != null && img.Name == "img" + ShowedNum.ToString())
                            {
                                img.Visibility = System.Windows.Visibility.Hidden;
                                ShowedNum++;
                                break;
                            }
                        }


                        if (ShowedNum <= 21)
                            timer.Enabled = true;


                    }));

            };


            timer.Interval = 1000;
            timer.AutoReset = false;
            timer.Start();
        }



        private void btnGoNext_Click(object sender, RoutedEventArgs e)
        {
            gMain.Visibility = System.Windows.Visibility.Collapsed;
            gSelect.Visibility = System.Windows.Visibility.Collapsed;

            gDraw.Visibility = System.Windows.Visibility.Visible;
            win.Focus();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            gDraw.Visibility = System.Windows.Visibility.Collapsed;
            gSelect.Visibility = System.Windows.Visibility.Collapsed;


            gMain.Visibility = System.Windows.Visibility.Visible;
            win.Focus();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.G)
            {
                if (gSelect.Visibility == System.Windows.Visibility.Visible)
                {
                    try
                    {
                        HidenNumbers();
                        string nm = txt1.Text + txt2.Text + txt3.Text + txt4.Text + txt5.Text;
                        next = int.Parse(nm);

                        if (next > dt.Rows.Count)
                            throw new Exception();

                        gSelect.Visibility = System.Windows.Visibility.Collapsed;
                        gDraw.Visibility = System.Windows.Visibility.Visible;
                        btnRandom_Click(null, null);
                    }
                    catch
                    {
                        MessageBox.Show("شماره ردیف وارد شده صحیح نمی باشد!" + System.Environment.NewLine
                            + "و یا قبلا انتخاب شده است."
                            + (dt == null ? "" :  System.Environment.NewLine + "از" + " 0" + " تا " + dt.Rows.Count));

                        
                    }

                }
                else
                {
                    HidenNumbers();
                    btnRandom_Click(null, null);
                }
            }
            else if (e.Key == Key.H)
                HidenNumbers();
            else if (e.Key == Key.M)
                btnBack_Click(null, null);

            else if (e.Key == Key.R)
                btnGoNext_Click(null, null);

            else if (e.Key == Key.D)
            {
                btnGoManual_Click(null, null);
                e.Handled = true;
            }
            else if(e.Key == Key.W)
            {
                string app = "";
                int i = 1;
                foreach(string s in lstSelected)
                {
                    app += i.ToString() + ": " + s + System.Environment.NewLine + System.Environment.NewLine;

                    i++;
                }

                MessageBox.Show(app);
            }



            win.Focus();
        }

        private void txt1_KeyDown(object sender, KeyEventArgs e)
        {
            // 74 84 numbers
            //34 43
            int code = (int)e.Key;
            if(e.Key == Key.G || e.Key == Key.M || e.Key == Key.R || e.Key == Key.W)
            {
                Window_KeyDown(null, e);
                e.Handled = true;
            } 
            else if (e.Key != Key.Tab && (( code < 74 || code > 83 ) && (code < 34 || code > 43)))
                e.Handled = true;
        }

        private void btnGoManual_Click(object sender, RoutedEventArgs e)
        {
            gMain.Visibility = System.Windows.Visibility.Collapsed;
            gDraw.Visibility = System.Windows.Visibility.Collapsed;


            gSelect.Visibility = System.Windows.Visibility.Visible;
            win.Focus();

           // txt1.Focus();
            txt1.Text = txt2.Text = txt3.Text = txt4.Text = txt5.Text = "";
            txt1.Focus();
        }

        private void txt4_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = sender as TextBox;
            box.Text = "";
        }
    }
}
